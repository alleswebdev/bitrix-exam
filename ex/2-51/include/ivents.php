<?
AddEventHandler("main", "OnBeforeEventAdd", array("MyClass", "OnBeforeEventAddHandler"));
class MyClass
{
    function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        global $USER;
        
         if ($USER->IsAuthorized()) {
              $arFields['AUTHOR'] = 'Пользователь авторизован, айди и вся херня:' ."[".$USER->GetID()."] (".$USER->GetLogin().") ".$USER->GetFullName() .
            'имя с формы:' . $arFields['AUTHOR'];
         } else{
            $arFields['AUTHOR'] = 'Пользователь неавторизован' .  $arFields['AUTHOR'];
         }

         CEventLog::Add(array(
         "SEVERITY" => "SECURITY",
         "AUDIT_TYPE_ID" => "CHANGE_FORM",
         "MODULE_ID" => "main",
         "ITEM_ID" => $USER->GetID(),
         "DESCRIPTION" => "Поменяли" . $arFields['AUTHOR'],
      ));
    }
}