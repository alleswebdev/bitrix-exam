<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 180;
}

$arParams["IB_PROD"] = intval($arParams["IB_PROD"]);
$arParams["IB_NEW"] = intval($arParams["IB_NEW"]);
$arParams["UF_CODE"] = trim($arParams["UF_CODE"]);

if ($this->StartResultCache()) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }
    
    $arSelect = array(
            "ID",
            "NAME",
            "DATE_ACTIVE_FROM",
            $arParams["UF_CODE"]
    );

    $arFilter = array(
            "IBLOCK_ID"=> $arParams["IB_PROD"],
            "!".$arParams["UF_CODE"] => 'false',
            "ACTIVE"=>"Y",
            "CNT_ACTIVE" => 'Y',
    );

    $rsSect = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
    while ($arSect = $rsSect->GetNext()) {
        $arResult['ITEMS_CNT'] += $arSect["ELEMENT_CNT"];
        $arResult['SECTIONS'][] = $arSect['ID'];
        foreach ($arSect['UF_NEWS_LINK'] as $k => $v) {
            $arResult['NEWS'][] = $v;
            $arResult['ITEMS'][$v]['SECT_ID'][] = $arSect['ID'];
            $arResult['ITEMS'][$v]['SECT_NAME'][] = $arSect['NAME'];
        }
    }

    $arSelect = array(
          "ID",
          "NAME",
          "PROPERTY_ARTNUMBER",
          "PROPERTY_MATERIAL",
          "PROPERTY_PRICE",
          "IBLOCK_SECTION_ID",
     );
    $arFilter = array(
      "IBLOCK_ID"=>$arParams["IB_PROD"],
      "SECTION_ID" => $arResult['SECTIONS'],
    );
    // сортировка здесь чёбы поулчить мин и макс цену)))0
    $res = CIBlockElement::GetList(array("PROPERTY_PRICE"=>"ASC"), $arFilter, false, false, $arSelect);
    
    while ($ob = $res->GetNext()) {
        $prices[] = $ob['PROPERTY_PRICE_VALUE'];
        foreach ($arResult['ITEMS'] as $k=>&$v) {
            if (in_array($ob['IBLOCK_SECTION_ID'], $v["SECT_ID"])) {
                $v["ELEMENTS"][$ob['ID']]['NAME'] = $ob['NAME'];
                $v["ELEMENTS"][$ob['ID']]['MATERIAL'] = $ob['PROPERTY_MATERIAL_VALUE'];
                $v["ELEMENTS"][$ob['ID']]['PRICE'] = $ob['PROPERTY_PRICE_VALUE'];
                $v["ELEMENTS"][$ob['ID']]['ARTNUMBER'] = $ob['PROPERTY_ARTNUMBER_VALUE'];
            }
        }
    }
    
    $arResult['MIN_PRICE'] = $prices[0];
    $arResult['MAX_PRICE'] = array_pop($prices);
    //var_dump($arResult['ITEMS']);
    $arResult['NEWS'] = array_unique($arResult['NEWS']);
    
    $arSelect = array(
      "ID",
      "NAME",
      "DATE_ACTIVE_FROM"
    );
    $arFilter = array(
      "IBLOCK_ID"=>$arParams["IB_NEW"],
      "ID" => $arResult['NEWS'],
    );
    // получаем имена привязанных новостей
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNext()) {
        $arResult['ITEMS'][$ob["ID"]]["NAME"] = $ob["NAME"];
        $arResult['ITEMS'][$ob["ID"]]["DATE"] = $ob["DATE_ACTIVE_FROM"];
    }
    
    if (count($arResult['ITEMS'])) {
        $this->SetResultCacheKeys(array('ITEMS_CNT')); // 'MIN_PRICE','MAX_PRICE' сетювью типа с шаблона норм кешируется
        $this->IncludeComponentTemplate();
    }
}
$APPLICATION->SetTitle("Количество элементов: ".$arResult['ITEMS_CNT']);
