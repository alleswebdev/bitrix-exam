<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IB_PROD" => array(
			"PARENT" => "BASE",
			"NAME" => 'инфоблок каталога',
			"TYPE" => "STRING",
			"DEFAULT" => "2",
		),
		"IB_NEW" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IB_NEWS_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"UF_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => 'код свойства',
			"TYPE" => "STRING",
			"DEFAULT" => "UF_NEWS_LINK",
		),
		"CACHE_TIME"  =>  array("DEFAULT"=>36000000),
	),
);

