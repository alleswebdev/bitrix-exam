<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="news-list">
    <h2> Каталог: </h2> 
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <ul>
            <li><?="<b>".$arItem["NAME"]."</b>-". $arItem["DATE"]."(".implode(', ', $arItem["SECT_NAME"]) .")"?> </li>
            <?foreach($arItem["ELEMENTS"] as $el):?>
                <li style="margin-left: 15px"><?= $el["NAME"] ."-". $el["PRICE"] ."-".  $el["MATERIAL"] .'-'.$el["ARTNUMBER"]?> </li>
            <?endforeach;?>
        </ul>
    <?endforeach;?>
</div>
<?$this->SetViewTarget('simplcomp');?>
  <div style="color:lightgreen; margin: 34px 15px 35px 15px"> Минимальная цена:<?=$arResult["MIN_PRICE"]?>  <br>Максимальная цена:<?=$arResult["MAX_PRICE"]?></div>
<?$this->EndViewTarget();?> 