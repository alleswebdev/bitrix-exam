<?
if ($APPLICATION->GetShowIncludeAreas())
{
$this->AddIncludeAreaIcons(
    Array( 
        Array(
            "ID" => "1",
            "TITLE" => "Название кнопки toolbar'a",
            "URL" => "ссылка для перехода",

            "HINT" => array( //тултип кнопки
                "TITLE" => "Заголовок тултипа",
                "TEXT" => "Текст тултипа" //HTML допускается
            ),
            "HINT_MENU" => array ( //тултип кнопки контекстного меню
                "TITLE" => "Заголовок тултипа",
                "TEXT" => "Текст тултипа" ,
            ),
            "IN_PARAMS_MENU" => true ,
            "IN_MENU" => false 
        )
    )
);
}